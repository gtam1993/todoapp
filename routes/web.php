<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $name=('WELCOME');
    return view('welcome',compact('name'));
});

Route::get('/contact','ContactController@contact')->name('contact-page');

Route::get('/home', 'HomePageController@home')->name('home-page');

Route::resource('todos','TodoController');